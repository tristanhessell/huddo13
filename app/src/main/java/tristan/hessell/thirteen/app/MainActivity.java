package tristan.hessell.thirteen.app;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity implements View.OnClickListener
{
    private MediaPlayer mediaPlayer;
    private ImageView huddo;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        mediaPlayer = MediaPlayer.create( this, R.raw.thirteen );
        mediaPlayer.setOnCompletionListener( new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion( MediaPlayer mp )
            {
                huddo.setImageResource( R.drawable.huddo );
            }
        } );

        huddo = (ImageView)findViewById( R.id.huddo );
        huddo.setOnClickListener( this );
    }

    @Override
    public void onStop()
    {
        super.onStop();

        //stop using the media resource if the activity
        //is getting oudda here
        if(mediaPlayer != null)
        {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public void onClick( View v )
    {
        huddo.setImageResource( R.drawable.huddo_hawth );
        mediaPlayer.start();
    }
}
