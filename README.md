# README #
### What is this repository for? ###
This repository is used to track the progress of the 13 app - based off of a meme for a group of friends (and eventually the world!).

### How do I get set up? ###
There are no extra requirements to get this set up - its just a simple android project with no external dependencies.

### Contribution guidelines ###
There are no guidelines to contributing to this project - I will be surprised if this ever become a collaborative effort.

### Who do I talk to? ###
If you need to contact me: tristanhessell@gmail.com